const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const { Client } = require('pg')

const client = new Client({
	user: 'postgres',
	host: 'localhost',
	password: 'Rochroch',
	database: 'Gamer'
})

client.connect()

class Wishlist {
  constructor () {
    this.createdAt = new Date()
    this.updatedAt = new Date()
    this.games = []
  }
}

/**Cette route retourne l'intégralité des 
 *  jeux présents dans la BDD
 */
router.get('/games', async(req, res) => {

  const result = await client.query({
    text: 'SELECT * FROM games2'    
  })

  res.json(result.rows)
})

/**Cette route retourne l'intégralité des 
 *  consoles présentent dans la BDD
 */
router.get('/consols', async(req, res) => {

  const result = await client.query({
    text: 'SELECT * FROM consol'    
  })

  res.json(result.rows)
})


/**
 * Cette roue ajoute un nouvel article à la base de données
 */
router.post('/game', async(req, res) => {
  const name = req.body.name
  const image = req.body.image
  const description = req.body.description
  const gameplay = req.body.gameplay
  const lienachat = req.body.lienachat

  if (typeof name !== 'string' || name === '' ||
      typeof description !== 'string' || description === '' ||
      typeof image !== 'string' || image === '' ||
      typeof gameplay !== 'string' || gameplay === '' ||
      typeof lienachat !== 'string' || lienachat === '') {
    res.status(400).json({ message: 'bad request' })
    return
  }

  const result1 = await client.query({
    text: 'SELECT * FROM games2 WHERE name=$1',
    values: [name]
  })

  if (result1.rows.length > 0) {
    res.status(401).json({
      message: 'game already exists'
    })
    return
  }

  const result = await client.query({
    text: 'INSERT INTO games2 (Name, Description, Image, gamePlay, lienachat) VALUES ($1, $2, $3, $4, $5) RETURNING *',
    values: [name, description, image, gameplay, lienachat]
  })

  const id = result.rows[0].id

  // on envoie le jeu ajouté à l'utilisateur
  res.json({
    id: id,
    name: name,
    description: description,
    image: image,
    gameplay: gameplay,
    lienachat: lienachat
  })

})

/**
 *
 */
router.post('/consol', async(req, res) => {
  const name = req.body.name
  const image = req.body.image
  const description = req.body.description

  if (typeof name !== 'string' || name === '' ||
      typeof description !== 'string' || description === '' ||
      typeof image !== 'string' || image === '' ) {
    res.status(400).json({ message: 'bad request' })
    return
  }

  const result1 = await client.query({
    text: 'SELECT * FROM consol WHERE name=$1',
    values: [name]
  })

  if (result1.rows.length > 0) {
    res.status(401).json({
      message: 'consol already exists'
    })
    return
  }

  const result = await client.query({
    text: 'INSERT INTO consol (Name, Description, Image) VALUES ($1, $2, $3) RETURNING *',
    values: [name, description, image]
  })

  const id = result.rows[0].id

  // on envoie le jeu ajouté à l'utilisateur
  res.json({
    id: id,
    name: name,
    description: description,
    image: image
  })

})

/**
 * Cette route met à jour un jeu modifié
 */
router.put('/game/:gameId', async(req, res) => {
   const gameId = parseInt(req.params.gameId)

  // si gameId n'est pas un nombre (NaN = Not A Number), alors on s'arrête
  if (isNaN(gameId)) {
    res.status(400).json({ message: 'gameId should be a number' })
    return
  }
  
  req.gameId = gameId

  const name = req.body.name
    const description = req.body.description
    const image = req.body.image
    const gameplay = req.body.gameplay
    const lienachat = req.body.lienachat

    await client.query({
      text: 'UPDATE games2 SET name=$1, description=$2, image=$3, gameplay=$4, lienachat=$5 WHERE id=$6',
      values: [name, description, image, gameplay, lienachat, req.gameId]
    })
    res.send()
})

//Met à jour la console
router.put('/consol/:consolId', async(req, res) => {
   const consolId = parseInt(req.params.consolId)

  // si gameId n'est pas un nombre (NaN = Not A Number), alors on s'arrête
  if (isNaN(consolId)) {
    res.status(400).json({ message: 'consolId should be a number' })
    return
  }
  
  req.consolId = consolId

  const name = req.body.name
    const description = req.body.description
    const image = req.body.image

    await client.query({
      text: 'UPDATE consol SET name=$1, description=$2, image=$3 WHERE id=$4',
      values: [name, description, image, req.consolId]
    })
    res.send()
})


//Supprime un jeu
router.delete('/game/:gameId', async(req, res) => {
    const gameId = parseInt(req.params.gameId)

      // si gameId n'est pas un nombre (NaN = Not A Number), alors on s'arrête
      if (isNaN(gameId)) {
        res.status(400).json({ message: 'gameId should be a number' })
        return
      }
  
     req.gameId = gameId

     await client.query({
       text: 'DELETE FROM games2 WHERE id=$1',
       values: [req.gameId]
     })

     res.send()
})

//Supprime une console
router.delete('/consol/:consolId', async(req, res) => {
    const consolId = parseInt(req.params.consolId)

      // si consolId n'est pas un nombre (NaN = Not A Number), alors on s'arrête
      if (isNaN(consolId)) {
        res.status(400).json({ message: 'consolId should be a number' })
        return
      }
  
     req.consolId = consolId

     await client.query({
       text: 'DELETE FROM consol WHERE id=$1',
       values: [req.consolId]
     })

     res.send()
})


/**
 * Notre mécanisme de sauvegarde des paniers des utilisateurs sera de simplement leur attribuer un panier grâce à req.session, sans authentification particulière
 */
router.use((req, res, next) => {
  // l'utilisateur n'est pas reconnu, lui attribuer un panier dans req.session
  if (typeof req.session.wishlist === 'undefined') {
    req.session.wishlist = new Wishlist()
  }
  next()
})

/*
 * Cette route doit retourner le panier de l'utilisateur, grâce à req.session
 */
router.get('/wishlist', (req, res) => {
  res.json(req.session.wishlist)
})

/*
 * Cette route doit ajouter un article au panier, puis retourner le panier modifié à l'utilisateur
 * Le body doit contenir l'id de l'article, ainsi que la quantité voulue
 */
router.post('/wishlist', async(req, res) => {
  
  const gameId = parseInt(req.body.gameId)
  console.log(gameId)
  const result = await client.query({
    text: 'SELECT * FROM games2'    
  })
  const games = result.rows

  const game = games.find(game => game.id === gameId)

  if(!game) {
    res.status(404).json({ message: "The game does not exist"})
    return
  }

  const gameInWishList = req.session.wishlist.games.find(game => game.id === gameId)

  if (gameInWishList) {
    res.status(400).json({ message: "The game is already in your cart"})
    return
  }

  const newGame = {
    id: gameId
  }

  req.session.wishlist.games.push(newGame)

  res.send(newGame)
})

/*
 * Cette route supprime un jeu de la wishList
 */
router.delete('/wishlist/:gameId', (req, res) => {
  const gameId = parseInt(req.params.gameId)

  const gameInWishList = req.session.wishlist.games.findIndex(game => game.id === gameId)

  if (gameInWishList === -1) {
    res.status(400).json({ message: "The game is not currently in your wishlist"})
    return
  }

  req.session.wishlist.games.splice(gameInWishList, 1)
  res.send()
})


/*
 * Cette route est utilisé pour la connexion à la bdd
 * 
 */
 router.post('/register', async(req, res) => {
  const pseudo = req.body.pseudo
  const email = req.body.email
  const mdp = req.body.password

  const result = await client.query({
    text: 'SELECT id, email, password, pseudo FROM users WHERE pseudo=$1 OR email=$2',
    values: [pseudo, email]   
  })

  if(result.rows.length > 0){
    res.status(400).json({ message: 'Utilisateur existant!!'})
    return
  }

  const hash = await bcrypt.hash(mdp, 10)

   const inst = await client.query({
    text: 'INSERT INTO users (email, password, pseudo) VALUES ($1,$2,$3)',
    values: [email,hash, pseudo]
  })

   res.json('Insertion réussie')

  
 })

  router.post('/login', async(req, res) => {
  const email = req.body.email
  const mdp = req.body.password

  const result = await client.query({
    text: 'SELECT id, pseudo, email, password FROM users WHERE email=$1',
    values: [email]   
  })

  if(result.rows.length === 0){
    res.status(400).json({ message: 'Utilisateur inexistant!!'})
    return
  }

  //res.json(result.rows[0].password)
  
  if(!await bcrypt.compare(mdp, result.rows[0].password)){
    res.status(401).json({ message: 'mot de passe incorrect'})
    return
  }

  if(req.session.userId){
    res.status(402).json({ message: 'Déjà connecté'})
    return
  }

  req.session.userId = result.rows[0].id

  res.json(result.rows[0])
 })


  router.get('/verif', async (req, res) => {

    if(req.session.userId){
      const result = await client.query({
      text: 'SELECT * FROM users WHERE id=$1',
      values: [req.session.userId]    
    })

    res.json(result.rows[0])
    }

    
  })

  router.get('/destroy', async (req, res) =>{
      if(req.session.userId){
        red.session.destroy
      }
  })




module.exports = router
