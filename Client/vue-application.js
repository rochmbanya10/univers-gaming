const Home = window.httpVueLoader('./components/Home.vue')
const Game = window.httpVueLoader('./components/Game.vue')
const Wishlist = window.httpVueLoader('./components/Wishlist.vue')
const Consol = window.httpVueLoader('./components/Consol.vue')
const Register = window.httpVueLoader('./components/Register.vue')
const Login = window.httpVueLoader('./components/Login.vue')
const Logout = window.httpVueLoader('./components/Logout.vue')


const routes = [
  { path: '/', component: Home },
  { path: '/game', component: Game},
  { path: '/wishlist', component: Wishlist},
  { path: '/consol', component: Consol},
   { path: '/register', component: Register},
  { path: '/login', component: Login},
  { path: '/logout', component: Logout},
]

const router = new VueRouter({
  routes
})

var app = new Vue({
  router,
  el: '#app',
  data: {
    games: [],
    consols: [],
    wishlist: {
      createdAt: null,
      updatedAt: null,
      games: []
    },
    user: {},
    isConnected: false,
  },

  async mounted () {
    const res = await axios.get('/api/games')
    this.games = res.data
    const res2 = await axios.get('/api/wishlist')
    this.wishlist = res2.data
    const res3 = await axios.get('/api/consols')
    this.consols = res3.data

    try {
      const res4 = await axios.get('/api/verif')
      this.user = res4.data
      this.isConnected = true
    } catch (err) {
      // if (err.response && err.response.statusCode === 401) {
      if (err.response?.status === 401) {
        this.isConnected = false
      } else {
        console.log('error', err)
      }
    }
    },

  methods: {
    async addGame (game) {
        const res = await axios.post('/api/game', game)
        //if(res.status === 200){
          alert('Le jeu a été correctement ajouté')
          this.games.push(res.data)
        //} 
    },

    async addConsol (consol) {
        const res = await axios.post('/api/consol', consol)
        //if(res.status === 200){
          alert('La console a été correctement ajouté')
          this.consols.push(res.data)
        //} 
    },

    async updateGame (newGame) {
      await axios.put('/api/game/' + newGame.id, newGame)
      const game = this.games.find(a => a.id === newGame.id)
      game.name = newGame.name
      game.description = newGame.description
      game.image = newGame.image
      game.gameplay = newGame.gameplay
      game.lienachat = newGame.lienachat
      alert('Le jeu a été correctement modifié');
    },

    async updateConsol (newConsol) {
      await axios.put('/api/consol/' + newConsol.id, newConsol)
      const consol = this.consols.find(a => a.id === newConsol.id)
      consol.name = newConsol.name
      consol.description = newConsol.description
      consol.image = newConsol.image
      alert('La console a été correctement modifiée');
    },

    async deleteGame (gameId){
       await axios.delete('/api/game/' + gameId)
      const index = this.games.findIndex(a => a.id === gameId)
      this.games.splice(index, 1)
      alert('Jeu supprimé avec succès!!')
    },

    async deleteConsol (consolId){
       await axios.delete('/api/consol/' + consolId)
      const index = this.consols.findIndex(a => a.id === consolId)
      this.consols.splice(index, 1)
      alert('Console supprimée avec succès!!')
    },

     async addToWishList (gameId) {
      const res = await axios.post('/api/wishlist', { gameId })
      this.wishlist.games.push(res.data)
    },

    async removeFromWishList (gameId) {
      const res = await axios.delete('/api/wishlist/' + gameId)
      const idx = this.wishlist.games.findIndex(a => a.id === gameId)
      this.wishlist.games.splice(idx, 1)
    },

    async login (user) {
      const res = await axios.post('/api/login', user)
      this.user = res.data
      this.isConnected = true
      this.$router.push('/game')
    },
  }
  })
